# Markdown Preview for FCC #

## This project should do the following when finished ##


1. Objective: Build a CodePen.io app that is functionally similar to this: https://codepen.io/FreeCodeCamp/full/JXrLLE/.

2. User Story: I can type GitHub-flavored Markdown into a text area.

3. User Story: I can see a preview of the output of my markdown that is updated as I type.

